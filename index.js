const express = require('express')
const app = express();
const port = 3000;
var bodyParser = require('body-parser');
const cors = require('cors');


app.use(cors());
app.use(bodyParser.json());

// POST /api/users gets JSON bodies
app.post('/', function (req, res) {
    res.send('Hello World!')
});

app.get('/', (req, res) => res.send('Hello World!'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`))